<?php

namespace App\Repositories;

use App\Models\PerformerByGroup;
use InfyOm\Generator\Common\BaseRepository;

class PerformerByGroupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PerformerByGroup::class;
    }
}
