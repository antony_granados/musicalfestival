<?php

namespace App\Repositories;

use App\Models\Concert;
use InfyOm\Generator\Common\BaseRepository;

class ConcertRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Concert::class;
    }
}
