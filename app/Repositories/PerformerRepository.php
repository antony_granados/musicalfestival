<?php

namespace App\Repositories;

use App\Models\Performer;
use InfyOm\Generator\Common\BaseRepository;

class PerformerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Performer::class;
    }
}
