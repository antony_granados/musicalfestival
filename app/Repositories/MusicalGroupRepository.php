<?php

namespace App\Repositories;

use App\Models\MusicalGroup;
use InfyOm\Generator\Common\BaseRepository;

class MusicalGroupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MusicalGroup::class;
    }
}
