<?php

namespace App\Repositories;

use App\Models\Genre;
use InfyOm\Generator\Common\BaseRepository;

class GenreRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Genre::class;
    }
}
