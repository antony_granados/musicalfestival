<?php

namespace App\Repositories;

use App\Models\Instrument;
use InfyOm\Generator\Common\BaseRepository;

class InstrumentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Instrument::class;
    }
}
