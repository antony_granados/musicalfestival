<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="PerformerByGroup",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="musical_group_id",
 *          description="musical_group_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="perfomer_id",
 *          description="perfomer_id",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class PerformerByGroup extends Model
{
    use SoftDeletes;

    public $table = 'performer_by_group';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'musical_group_id',
        'perfomer_id',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'musical_group_id' => 'integer',
        'perfomer_id' => 'integer',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function musicalGroup()
    {
        return $this->belongsTo('App\Models\MusicalGroup');
    }

    public function performer()
    {
        return $this->belongsTo('App\Models\Performer');
    }
}
