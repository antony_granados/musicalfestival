<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Performer",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),      
 *      @SWG\Property(
 *          property="instrument_id",
 *          description="instrument_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="musical_group_id",
 *          description="genre_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="image_url",
 *          description="image_url",
 *          type="string"
 *      )
 * )
 */
class Performer extends Model
{
    use SoftDeletes;

    public $table = 'performer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'last_name',        
        'instrument_id',
        'musical_group_id',
        'image_url',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'last_name' => 'string',        
        'instrument_id' => 'integer',
        'musical_group_id' => 'integer',
        'image_url' => 'string',
        'deleted_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function group()
    {
        return $this->belongsTo('App\Models\MusicalGroup');

    }

    public function instrument()
    {
        return $this->belongsTo('App\Models\Instrument');

    }
}
