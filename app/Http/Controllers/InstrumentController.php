<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateInstrumentRequest;
use App\Http\Requests\UpdateInstrumentRequest;
use App\Repositories\InstrumentRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class InstrumentController extends InfyOmBaseController
{
    /** @var  InstrumentRepository */
    private $instrumentRepository;

    public function __construct(InstrumentRepository $instrumentRepo)
    {
        $this->instrumentRepository = $instrumentRepo;
    }

    /**
     * Display a listing of the Instrument.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->instrumentRepository->pushCriteria(new RequestCriteria($request));
        $instruments = $this->instrumentRepository->all();

        return view('instruments.index')
            ->with('instruments', $instruments);
    }

    /**
     * Show the form for creating a new Instrument.
     *
     * @return Response
     */
    public function create()
    {
        return view('instruments.create');
    }

    /**
     * Store a newly created Instrument in storage.
     *
     * @param CreateInstrumentRequest $request
     *
     * @return Response
     */
    public function store(CreateInstrumentRequest $request)
    {
        $input = $request->all();

        $instrument = $this->instrumentRepository->create($input);

        Flash::success('Instrument saved successfully.');

        return redirect(route('instruments.index'));
    }

    /**
     * Display the specified Instrument.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $instrument = $this->instrumentRepository->findWithoutFail($id);

        if (empty($instrument)) {
            Flash::error('Instrument not found');

            return redirect(route('instruments.index'));
        }

        return view('instruments.show')->with('instrument', $instrument);
    }

    /**
     * Show the form for editing the specified Instrument.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $instrument = $this->instrumentRepository->findWithoutFail($id);

        if (empty($instrument)) {
            Flash::error('Instrument not found');

            return redirect(route('instruments.index'));
        }

        return view('instruments.edit')->with('instrument', $instrument);
    }

    /**
     * Update the specified Instrument in storage.
     *
     * @param  int              $id
     * @param UpdateInstrumentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInstrumentRequest $request)
    {
        $instrument = $this->instrumentRepository->findWithoutFail($id);

        if (empty($instrument)) {
            Flash::error('Instrument not found');

            return redirect(route('instruments.index'));
        }

        $instrument = $this->instrumentRepository->update($request->all(), $id);

        Flash::success('Instrument updated successfully.');

        return redirect(route('instruments.index'));
    }

    /**
     * Remove the specified Instrument from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $instrument = $this->instrumentRepository->findWithoutFail($id);

        if (empty($instrument)) {
            Flash::error('Instrument not found');

            return redirect(route('instruments.index'));
        }

        $this->instrumentRepository->delete($id);

        Flash::success('Instrument deleted successfully.');

        return redirect(route('instruments.index'));
    }
}
