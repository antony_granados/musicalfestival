<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\City;
use App\Models\Genre;
use App\Models\MusicalGroup;
use App\Models\Instrument;


class Utils
{ 
    /**
    *Get all Cities
    *
    * @return formatted array
    */
    public function getAllCities(){
        $cities = City::all('id', 'name');
        $citiesFormatted = [];
        foreach ($cities as $city) {
            $citiesFormatted[$city->id] = $city->name;
        }
        return $citiesFormatted;
    }

    /**
    *Get all Genres
    *
    * @return formatted array
    */
    public function getAllGenres(){
        $genres = Genre::all('id', 'name');
        $genresFormatted = [];
        foreach ($genres as $genre) {
            $genresFormatted[$genre->id] = $genre->name;
        }
        return $genresFormatted;
    }

    /**
    *Get all Instruments
    *
    * @return formatted array
    */
    public function getAllInstruments(){
        $instruments = Instrument::all('id', 'name');
        $instrumetsFormatted = [];
        foreach ($instruments as $instrument) {
            $instrumentsFormatted[$instrument->id] = $instrument->name;
        }
        return $instrumentsFormatted;
    }

    /**
    *Get all Groups
    *
    * @return formatted array
    */
    public function getAllGroups(){
        $groups = MusicalGroup::all('id', 'name');
        $groupsFormatted = [];
        foreach ($groups as $group) {
            $groupsFormatted[$group->id] = $group->name;
        }
        return $groupsFormatted;
    }
    
}
