<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePerformerByGroupRequest;
use App\Http\Requests\UpdatePerformerByGroupRequest;
use App\Repositories\PerformerByGroupRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PerformerByGroupController extends InfyOmBaseController
{
    /** @var  PerformerByGroupRepository */
    private $performerByGroupRepository;

    public function __construct(PerformerByGroupRepository $performerByGroupRepo)
    {
        $this->performerByGroupRepository = $performerByGroupRepo;
    }

    /**
     * Display a listing of the PerformerByGroup.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->performerByGroupRepository->pushCriteria(new RequestCriteria($request));
        $performerByGroups = $this->performerByGroupRepository->all();

        return view('performerByGroups.index')
            ->with('performerByGroups', $performerByGroups);
    }

    /**
     * Show the form for creating a new PerformerByGroup.
     *
     * @return Response
     */
    public function create()
    {
        return view('performerByGroups.create');
    }

    /**
     * Store a newly created PerformerByGroup in storage.
     *
     * @param CreatePerformerByGroupRequest $request
     *
     * @return Response
     */
    public function store(CreatePerformerByGroupRequest $request)
    {
        $input = $request->all();

        $performerByGroup = $this->performerByGroupRepository->create($input);

        Flash::success('PerformerByGroup saved successfully.');

        return redirect(route('performerByGroups.index'));
    }

    /**
     * Display the specified PerformerByGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $performerByGroup = $this->performerByGroupRepository->findWithoutFail($id);

        if (empty($performerByGroup)) {
            Flash::error('PerformerByGroup not found');

            return redirect(route('performerByGroups.index'));
        }

        return view('performerByGroups.show')->with('performerByGroup', $performerByGroup);
    }

    /**
     * Show the form for editing the specified PerformerByGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $performerByGroup = $this->performerByGroupRepository->findWithoutFail($id);

        if (empty($performerByGroup)) {
            Flash::error('PerformerByGroup not found');

            return redirect(route('performerByGroups.index'));
        }

        return view('performerByGroups.edit')->with('performerByGroup', $performerByGroup);
    }

    /**
     * Update the specified PerformerByGroup in storage.
     *
     * @param  int              $id
     * @param UpdatePerformerByGroupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePerformerByGroupRequest $request)
    {
        $performerByGroup = $this->performerByGroupRepository->findWithoutFail($id);

        if (empty($performerByGroup)) {
            Flash::error('PerformerByGroup not found');

            return redirect(route('performerByGroups.index'));
        }

        $performerByGroup = $this->performerByGroupRepository->update($request->all(), $id);

        Flash::success('PerformerByGroup updated successfully.');

        return redirect(route('performerByGroups.index'));
    }

    /**
     * Remove the specified PerformerByGroup from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $performerByGroup = $this->performerByGroupRepository->findWithoutFail($id);

        if (empty($performerByGroup)) {
            Flash::error('PerformerByGroup not found');

            return redirect(route('performerByGroups.index'));
        }

        $this->performerByGroupRepository->delete($id);

        Flash::success('PerformerByGroup deleted successfully.');

        return redirect(route('performerByGroups.index'));
    }
}
