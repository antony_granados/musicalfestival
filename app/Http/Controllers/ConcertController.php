<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateConcertRequest;
use App\Http\Requests\UpdateConcertRequest;
use App\Repositories\ConcertRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ConcertController extends InfyOmBaseController
{
    /** @var  ConcertRepository */
    private $concertRepository;

    public function __construct(ConcertRepository $concertRepo)
    {
        $this->concertRepository = $concertRepo;
    }

    /**
     * Display a listing of the Concert.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->concertRepository->pushCriteria(new RequestCriteria($request));
        $concerts = $this->concertRepository->all();

        return view('concerts.index')
            ->with('concerts', $concerts);
    }

    /**
     * Show the form for creating a new Concert.
     *
     * @return Response
     */
    public function create()
    {
        return view('concerts.create');
    }

    /**
     * Store a newly created Concert in storage.
     *
     * @param CreateConcertRequest $request
     *
     * @return Response
     */
    public function store(CreateConcertRequest $request)
    {
        $input = $request->all();

        $concert = $this->concertRepository->create($input);

        Flash::success('Concert saved successfully.');

        return redirect(route('concerts.index'));
    }

    /**
     * Display the specified Concert.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $concert = $this->concertRepository->findWithoutFail($id);

        if (empty($concert)) {
            Flash::error('Concert not found');

            return redirect(route('concerts.index'));
        }

        return view('concerts.show')->with('concert', $concert);
    }

    /**
     * Show the form for editing the specified Concert.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $concert = $this->concertRepository->findWithoutFail($id);

        if (empty($concert)) {
            Flash::error('Concert not found');

            return redirect(route('concerts.index'));
        }

        return view('concerts.edit')->with('concert', $concert);
    }

    /**
     * Update the specified Concert in storage.
     *
     * @param  int              $id
     * @param UpdateConcertRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConcertRequest $request)
    {
        $concert = $this->concertRepository->findWithoutFail($id);

        if (empty($concert)) {
            Flash::error('Concert not found');

            return redirect(route('concerts.index'));
        }

        $concert = $this->concertRepository->update($request->all(), $id);

        Flash::success('Concert updated successfully.');

        return redirect(route('concerts.index'));
    }

    /**
     * Remove the specified Concert from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $concert = $this->concertRepository->findWithoutFail($id);

        if (empty($concert)) {
            Flash::error('Concert not found');

            return redirect(route('concerts.index'));
        }

        $this->concertRepository->delete($id);

        Flash::success('Concert deleted successfully.');

        return redirect(route('concerts.index'));
    }
}
