<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMusicalGroupRequest;
use App\Http\Requests\UpdateMusicalGroupRequest;
use App\Repositories\MusicalGroupRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Controllers\Utils;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MusicalGroupController extends InfyOmBaseController
{
    /** @var  MusicalGroupRepository */
    private $musicalGroupRepository;
    private $utils;

    public function __construct(MusicalGroupRepository $musicalGroupRepo)
    {
        $this->musicalGroupRepository = $musicalGroupRepo; 
        $this->utils = new Utils();               
    }

    /**
     * Display a listing of the MusicalGroup.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->musicalGroupRepository->pushCriteria(new RequestCriteria($request));
        $musicalGroups = $this->musicalGroupRepository->all();

        return view('musicalGroups.index')
            ->with('musicalGroups', $musicalGroups);
    }

    /**
     * Show the form for creating a new MusicalGroup.
     *
     * @return Response
     */
    public function create()
    {                   
        return view('musicalGroups.create')
            ->with('cities', $this->utils->getAllCities()) 
            ->with('genres', $this->utils->getAllGenres());            
    }

    /**
     * Store a newly created MusicalGroup in storage.
     *
     * @param CreateMusicalGroupRequest $request
     *
     * @return Response
     */
    public function store(CreateMusicalGroupRequest $request)
    {        
        $input = $request->all();
        $filePath = "http://localhost/musicalfestival/public/assets/images/musicalgroups/";

        if($request->hasFile('image_url')) {
            $file = $request->file('image_url');
            $name = $file->getClientOriginalName();                                
            $file->move(public_path().'/assets/images/musicalgroups/', $name);

            $input['image_url'] = $filePath . $name;
        }

        $musicalGroup = $this->musicalGroupRepository->create($input);        

        Flash::success('MusicalGroup saved successfully.');

        return redirect(route('musicalGroups.index'));
    }

    /**
     * Display the specified MusicalGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $musicalGroup = $this->musicalGroupRepository->findWithoutFail($id);

        if (empty($musicalGroup)) {
            Flash::error('MusicalGroup not found');

            return redirect(route('musicalGroups.index'));
        }

        return view('musicalGroups.show')->with('musicalGroup', $musicalGroup);
    }

    /**
     * Show the form for editing the specified MusicalGroup.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $musicalGroup = $this->musicalGroupRepository->findWithoutFail($id);

        if (empty($musicalGroup)) {
            Flash::error('MusicalGroup not found');

            return redirect(route('musicalGroups.index'));
        }

        return view('musicalGroups.edit')->with('musicalGroup', $musicalGroup);
    }

    /**
     * Update the specified MusicalGroup in storage.
     *
     * @param  int              $id
     * @param UpdateMusicalGroupRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMusicalGroupRequest $request)
    {
        $musicalGroup = $this->musicalGroupRepository->findWithoutFail($id);

        if (empty($musicalGroup)) {
            Flash::error('MusicalGroup not found');

            return redirect(route('musicalGroups.index'));
        }

        $musicalGroup = $this->musicalGroupRepository->update($request->all(), $id);

        Flash::success('MusicalGroup updated successfully.');

        return redirect(route('musicalGroups.index'));
    }

    /**
     * Remove the specified MusicalGroup from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $musicalGroup = $this->musicalGroupRepository->findWithoutFail($id);

        if (empty($musicalGroup)) {
            Flash::error('MusicalGroup not found');

            return redirect(route('musicalGroups.index'));
        }

        $this->musicalGroupRepository->delete($id);

        Flash::success('MusicalGroup deleted successfully.');

        return redirect(route('musicalGroups.index'));
    }    
    
}
