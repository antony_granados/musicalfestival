<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePerformerRequest;
use App\Http\Requests\UpdatePerformerRequest;
use App\Repositories\PerformerRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use App\Http\Controllers\Utils;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PerformerController extends InfyOmBaseController
{
    /** @var  PerformerRepository */
    private $performerRepository;

    public function __construct(PerformerRepository $performerRepo)
    {
        $this->performerRepository = $performerRepo;
        $this->utils = new Utils();
    }

    /**
     * Display a listing of the Performer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->performerRepository->pushCriteria(new RequestCriteria($request));
        $performers = $this->performerRepository->all();

        return view('performers.index')
            ->with('performers', $performers);
    }

    /**
     * Show the form for creating a new Performer.
     *
     * @return Response
     */
    public function create()
    {
        return view('performers.create')
            ->with('instruments', $this->utils->getAllInstruments())
            ->with('groups', $this->utils->getAllGroups());
    }

    /**
     * Store a newly created Performer in storage.
     *
     * @param CreatePerformerRequest $request
     *
     * @return Response
     */
    public function store(CreatePerformerRequest $request)
    {
        $input = $request->all();

        $filePath = "http://localhost/musicalfestival/public/assets/images/performer/";

        if($request->hasFile('image_url')) {
            $file = $request->file('image_url');
            $name = $file->getClientOriginalName();                                
            $file->move(public_path().'/assets/images/performer/', $name);

            $input['image_url'] = $filePath . $name;
        }

        $performer = $this->performerRepository->create($input);

        Flash::success('Performer saved successfully.');

        return redirect(route('performers.index'));
    }

    /**
     * Display the specified Performer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $performer = $this->performerRepository->findWithoutFail($id);

        if (empty($performer)) {
            Flash::error('Performer not found');

            return redirect(route('performers.index'));
        }

        return view('performers.show')->with('performer', $performer);
    }

    /**
     * Show the form for editing the specified Performer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $performer = $this->performerRepository->findWithoutFail($id);

        if (empty($performer)) {
            Flash::error('Performer not found');

            return redirect(route('performers.index'));
        }

        return view('performers.edit')->with('performer', $performer);
    }

    /**
     * Update the specified Performer in storage.
     *
     * @param  int              $id
     * @param UpdatePerformerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePerformerRequest $request)
    {
        $performer = $this->performerRepository->findWithoutFail($id);

        if (empty($performer)) {
            Flash::error('Performer not found');

            return redirect(route('performers.index'));
        }

        $performer = $this->performerRepository->update($request->all(), $id);

        Flash::success('Performer updated successfully.');

        return redirect(route('performers.index'));
    }

    /**
     * Remove the specified Performer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $performer = $this->performerRepository->findWithoutFail($id);

        if (empty($performer)) {
            Flash::error('Performer not found');

            return redirect(route('performers.index'));
        }

        $this->performerRepository->delete($id);

        Flash::success('Performer deleted successfully.');

        return redirect(route('performers.index'));
    }
}
