@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Concert</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($concert, ['route' => ['concerts.update', $concert->id], 'method' => 'patch']) !!}

            @include('concerts.fields')

            {!! Form::close() !!}
        </div>
@endsection
