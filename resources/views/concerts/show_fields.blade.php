<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $concert->id !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $concert->description !!}</p>
</div>

<!-- Concert Date Field -->
<div class="form-group">
    {!! Form::label('concert_date', 'Concert Date:') !!}
    <p>{!! $concert->concert_date !!}</p>
</div>

<!-- City Id Field -->
<div class="form-group">
    {!! Form::label('city_id', 'City Id:') !!}
    <p>{!! $concert->city_id !!}</p>
</div>

<!-- Musical Group Id Field -->
<div class="form-group">
    {!! Form::label('musical_group_id', 'Musical Group Id:') !!}
    <p>{!! $concert->musical_group_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $concert->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $concert->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $concert->deleted_at !!}</p>
</div>

