<table class="table table-responsive" id="concerts-table">
    <thead>
        <th>Description</th>
        <th>Concert Date</th>
        <th>City Id</th>
        <th>Musical Group Id</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th>Deleted At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($concerts as $concert)
        <tr>
            <td>{!! $concert->description !!}</td>
            <td>{!! $concert->concert_date !!}</td>
            <td>{!! $concert->city_id !!}</td>
            <td>{!! $concert->musical_group_id !!}</td>
            <td>{!! $concert->created_at !!}</td>
            <td>{!! $concert->updated_at !!}</td>
            <td>{!! $concert->deleted_at !!}</td>
            <td>
                {!! Form::open(['route' => ['concerts.destroy', $concert->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('concerts.show', [$concert->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('concerts.edit', [$concert->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
