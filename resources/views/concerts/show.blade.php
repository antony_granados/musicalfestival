@extends('layouts.app')

@section('content')
    @include('concerts.show_fields')

    <div class="form-group">
           <a href="{!! route('concerts.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
