@extends('layouts.app')                

@section('content')
    <section id="home"> 
        <div id="main-slider" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <img class="img-responsive" src="http://localhost/musicalfestival/public/assets/images/slider/bg1.jpg" alt="slider">                       
                    <div class="carousel-caption">
                        <h2>First LOLAPALOOZA FESTIVAL COSTA RICA</h2>
                        <h4>A lot of artists invited</h4>
                        <a href="">View artists<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                <div class="item">
                    <img class="img-responsive" src="http://localhost/musicalfestival/public/assets/images/slider/bg2.jpg" alt="slider">   
                    <div class="carousel-caption">
                        <h2>Concerts on all provinces! </h2>                                                
                    </div>
                </div>
                <div class="item">
                    <img class="img-responsive" src="http://localhost/musicalfestival/public/assets/images/slider/bg3.jpg" alt="slider">   
                    <div class="carousel-caption">
                        <h2>Register new performers!</h2>
                        <h4>Do you know other artists in the festival?</h4>                        
                    </div>
                </div>              
            </div>
        </div>      
    </section>
    <!--/#home-->
@endsection

