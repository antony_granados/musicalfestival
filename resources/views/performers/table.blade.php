<div class="carousel-inner">
    <?php
        for($i=0; $i < count($performers); $i++){
            $mod = $i % 3;

            if(($i !== 0) && ($mod === 0)){
                echo('
                        </div>
                    </div>');
            }

            if(($i === 0) || ($mod === 0)) {
                $class = ($i === 0) ? "item active" : "item";
                echo('
                    <div class="' . $class .'">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="single-event">
                                    <img class="img-responsive" src="' . $performers[$i]->image_url . '" alt="event-image">
                                    <h4>' . $performers[$i]->name . '</h4>
                                    <h5>' . $performers[$i]->instrument->name . '</h5>
                                </div>
                            </div>
                    ');
            } else {
                echo('
                    <div class="col-sm-4">
                        <div class="single-event">
                            <img class="img-responsive" src="' . $performers[$i]->image_url . '" alt="event-image">
                            <h4>' . $performers[$i]->name . '</h4>
                            <h5>' . $performers[$i]->instrument->name . '</h5>
                        </div>
                    </div>
                    ');
            }

            if($i === count($performers) -1){
                echo('
                        </div>
                    </div>');
            }        
        }
    ?>
</div>
