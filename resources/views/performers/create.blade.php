@extends('layouts.app')

@section('content')
    <section id="contact">        
        <div class="contact-section" style="padding-top:140px">
            <div class="ear-piece">
                <img class="img-responsive" src="images/ear-piece.png" alt="">
            </div>
            <div class="container">
                <div class="row">                    
                    <div class="col-sm-12">
                        <div id="contact-section">
                            <h2>Create New Performer</h2>
                            <div class="status alert alert-success" style="display: none"></div>
                            @include('core-templates::common.errors')
                            <div id="main-contact-form" class="contact-form">
                                {!! Form::open(['route' => 'performers.store', 'files'=>true]) !!}

                                    @include('performers.fields')

                                {!! Form::close() !!}                                
                            </div>        
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </section>
    <!--/#contact-->
@endsection
