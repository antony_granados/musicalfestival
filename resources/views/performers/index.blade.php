@extends('layouts.app')

@section('content')
	<section id="event">
	    <div class="container" style="padding-top:180px">
	        <div class="row">
	            <div class="col-sm-12 col-md-9">
	                <div id="event-carousel" class="carousel slide" data-interval="false">
	                    <h2 class="heading">Performers</h2>	                    
	                    <a class="btn btn-primary" href="{!! route('performers.create') !!}">Add New</a>	                    
	                    <a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
	                    <a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
	                    @include('performers.table')
	                </div>
	            </div>
	            <div class="guitar">
	                <img class="img-responsive" src="http://localhost/musicalfestival/public/assets/images/guitar.png" alt="guitar">
	            </div>
	        </div>          
	    </div>
	</section><!--/#event-->
@endsection