<div class="form-group">
    {!! Form::select('musical_group_id', $groups, 1, ['class' => 'form-control']) !!}    
</div>
<div class="form-group">    
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Nombre']) !!}
</div>
<div class="form-group">
    {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder'=>'Apellido']) !!}
</div>
<div class="form-group">
    {!! Form::select('instrument_id', $instruments, 1, ['class' => 'form-control']) !!}    
</div>
<div class="form-group">
    {!! Form::file('image_url', ['class' => 'form-control', 'style' => 'background:none;border:none']) !!}
</div>                        
<div class="form-group">    
    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
    <a href="{!! route('musicalGroups.index') !!}" class="btn btn-primary">Cancel</a>
</div>
