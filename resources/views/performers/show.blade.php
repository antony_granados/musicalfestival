@extends('layouts.app')

@section('content')
    @include('performers.show_fields')

    <div class="form-group">
           <a href="{!! route('performers.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
