@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Performer</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($performer, ['route' => ['performers.update', $performer->id], 'method' => 'patch']) !!}

            @include('performers.fields')

            {!! Form::close() !!}
        </div>
@endsection
