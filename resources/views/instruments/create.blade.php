@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="pull-left">Create New Instrument</h1>
        </div>
    </div>

    @include('core-templates::common.errors')

    <div class="row">
        {!! Form::open(['route' => 'instruments.store']) !!}

            @include('instruments.fields')

        {!! Form::close() !!}
    </div>
@endsection
