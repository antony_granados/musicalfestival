@extends('layouts.app')

@section('content')
    @include('instruments.show_fields')

    <div class="form-group">
           <a href="{!! route('instruments.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
