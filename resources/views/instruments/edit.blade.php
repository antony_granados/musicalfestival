@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Instrument</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($instrument, ['route' => ['instruments.update', $instrument->id], 'method' => 'patch']) !!}

            @include('instruments.fields')

            {!! Form::close() !!}
        </div>
@endsection
