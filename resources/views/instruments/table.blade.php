<table class="table table-responsive" id="instruments-table">
    <thead>
        <th>Name</th>
        <th>Type</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th>Deleted At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($instruments as $instrument)
        <tr>
            <td>{!! $instrument->name !!}</td>
            <td>{!! $instrument->type !!}</td>
            <td>{!! $instrument->created_at !!}</td>
            <td>{!! $instrument->updated_at !!}</td>
            <td>{!! $instrument->deleted_at !!}</td>
            <td>
                {!! Form::open(['route' => ['instruments.destroy', $instrument->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('instruments.show', [$instrument->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('instruments.edit', [$instrument->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
