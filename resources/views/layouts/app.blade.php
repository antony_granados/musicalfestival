<!DOCTYPE html>
<html lang="en">
    <head>    
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Festival Musical Test">
        <meta name="author" content="Antony Granados">
        <title>LolaPalooza</title>

        <!-- Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
              type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

        <!-- Bootstrap -->
        <link href="http://localhost/musicalfestival/public/assets/css/bootstrap.min.css" rel="stylesheet">                
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.1/css/bootstrap-toggle.min.css">                
        
        <!-- Main -->
        <link href="http://localhost/musicalfestival/public/assets/css/main.css" rel="stylesheet">
        <link href="http://localhost/musicalfestival/public/assets/css/animate.css" rel="stylesheet">

        <!-- Responsive -->
        <link href="http://localhost/musicalfestival/public/assets/css/responsive.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="http://localhost/musicalfestival/public/assets/js/html5shiv.js"></script>
            <script src="http://localhost/musicalfestival/public/assets/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="http://localhost/musicalfestival/public/assets/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://localhost/musicalfestival/public/assets/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://localhost/musicalfestival/public/assets/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://localhost/musicalfestival/public/assets/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="http://localhost/musicalfestival/public/assets/images/ico/apple-touch-icon-57-precomposed.png">    
    </head>
    <body id="app-layout">
        <header id="header" role="banner">
        @if (Auth::guest())      
            <div class="main-nav">
                <div class="container">
                    <div class="header-top">
                        <div class="pull-right social-icons">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-youtube"></i></a>
                        </div>
                    </div>     
                    <div class="row">                   
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{ url('/') }}">
                                <img class="img-responsive" src="http://localhost/musicalfestival/public/assets/images/logo.png" alt="logo">
                            </a>                    
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="{{ url('/home') }}">Home</a></li>
                                <li><a href="{{ url('/musicalGroups') }}">Musical Groups</a></li>                                                         
                                <li><a href="{{ url('/performers') }}">Performers</a></li>
                                <!--@if (Auth::guest())
                                    <li><a href="{{ url('/login') }}">Login</a></li>
                                    <li><a href="{{ url('/register') }}">Register</a></li>
                                @endif-->       
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div id="wrapper" class="">
                <!-- Sidebar -->
                    @include('layouts.sidebar')
                <!-- /#sidebar-wrapper -->
                <header class="header">
                    <a href="#menu-toggle"
                       style="margin-top: 8px;margin-left: 5px;background-color: #E7E7E7;border-color: #E7E7E7"
                       class="btn btn-default" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>

                    @if (!Auth::guest())
                        <span class="pull-right" style="margin-right: 10px;margin-top: 15px"><a href="{{ url('/logout') }}"><i
                                        class="fa fa-btn fa-sign-out"></i>Logout</a></span>
                    @endif
                </header>
            </div>
        @endif                    
        </header>
        <!--/#header-->


        <!-- Page Content -->
        @yield('content')
        <!-- /#page-content-wrapper -->

        <!-- JavaScripts -->
        <script type="text/javascript" src="http://localhost/musicalfestival/public/assets/js/jquery.js"></script>
        <script type="text/javascript" src="http://localhost/musicalfestival/public/assets/js/bootstrap.min.js"></script>        
        <script type="text/javascript" src="http://localhost/musicalfestival/public/assets/js/jquery.parallax.js"></script>        
        <!--
        <script type="text/javascript" src="http://localhost/musicalfestival/public/assets/js/smoothscroll.js"></script>        
        <script type="text/javascript" src="http://localhost/musicalfestival/public/assets/js/jquery.scrollTo.js"></script>
        <script type="text/javascript" src="http://localhost/musicalfestival/public/assets/js/jquery.nav.js"></script> 
        -->
        <script type="text/javascript" src="http://localhost/musicalfestival/public/assets/js/main.js"></script>                
        <!--
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.1/js/bootstrap-toggle.min.js"></script> -->              

        @yield('scripts')

    </body>
</html>
