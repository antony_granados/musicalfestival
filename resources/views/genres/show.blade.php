@extends('layouts.app')

@section('content')
    @include('genres.show_fields')

    <div class="form-group">
           <a href="{!! route('genres.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
