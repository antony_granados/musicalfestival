<table class="table table-responsive" id="genres-table">
    <thead>
        <th>Name</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th>Deleted At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($genres as $genre)
        <tr>
            <td>{!! $genre->name !!}</td>
            <td>{!! $genre->created_at !!}</td>
            <td>{!! $genre->updated_at !!}</td>
            <td>{!! $genre->deleted_at !!}</td>
            <td>
                {!! Form::open(['route' => ['genres.destroy', $genre->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('genres.show', [$genre->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('genres.edit', [$genre->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
