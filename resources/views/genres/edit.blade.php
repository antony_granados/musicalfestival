@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Genre</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($genre, ['route' => ['genres.update', $genre->id], 'method' => 'patch']) !!}

            @include('genres.fields')

            {!! Form::close() !!}
        </div>
@endsection
