<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Musical Group Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('musical_group_id', 'Musical Group Id:') !!}
    {!! Form::number('musical_group_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Perfomer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('perfomer_id', 'Perfomer Id:') !!}
    {!! Form::number('perfomer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Created At:') !!}
    {!! Form::date('created_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Updated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('updated_at', 'Updated At:') !!}
    {!! Form::date('updated_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Deleted At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    {!! Form::date('deleted_at', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('performerByGroups.index') !!}" class="btn btn-default">Cancel</a>
</div>
