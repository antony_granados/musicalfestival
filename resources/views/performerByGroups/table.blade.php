<table class="table table-responsive" id="performerByGroups-table">
    <thead>
        <th>Musical Group Id</th>
        <th>Perfomer Id</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th>Deleted At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($performerByGroups as $performerByGroup)
        <tr>
            <td>{!! $performerByGroup->musical_group_id !!}</td>
            <td>{!! $performerByGroup->perfomer_id !!}</td>
            <td>{!! $performerByGroup->created_at !!}</td>
            <td>{!! $performerByGroup->updated_at !!}</td>
            <td>{!! $performerByGroup->deleted_at !!}</td>
            <td>
                {!! Form::open(['route' => ['performerByGroups.destroy', $performerByGroup->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('performerByGroups.show', [$performerByGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('performerByGroups.edit', [$performerByGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
