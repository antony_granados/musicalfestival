@extends('layouts.app')

@section('content')
    @include('performerByGroups.show_fields')

    <div class="form-group">
           <a href="{!! route('performerByGroups.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection
