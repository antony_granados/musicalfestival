@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit PerformerByGroup</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($performerByGroup, ['route' => ['performerByGroups.update', $performerByGroup->id], 'method' => 'patch']) !!}

            @include('performerByGroups.fields')

            {!! Form::close() !!}
        </div>
@endsection
