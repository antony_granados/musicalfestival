<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $performerByGroup->id !!}</p>
</div>

<!-- Musical Group Id Field -->
<div class="form-group">
    {!! Form::label('musical_group_id', 'Musical Group Id:') !!}
    <p>{!! $performerByGroup->musical_group_id !!}</p>
</div>

<!-- Perfomer Id Field -->
<div class="form-group">
    {!! Form::label('perfomer_id', 'Perfomer Id:') !!}
    <p>{!! $performerByGroup->perfomer_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $performerByGroup->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $performerByGroup->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $performerByGroup->deleted_at !!}</p>
</div>

