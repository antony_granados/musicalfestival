@extends('layouts.app')

@section('content')
	<section id="event">
	    <div class="container" style="padding-top:180px">
	    	<!--<div class="row">
	    		<div class="col_sm_12">
	    			<div class="dropdown">
					  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					    Grupos por region
					    <span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    <li><a href="#">Action</a></li>
					    <li><a href="#">Another action</a></li>
					    <li><a href="#">Something else here</a></li>
					    <li role="separator" class="divider"></li>
					    <li><a href="#">Separated link</a></li>
					  </ul>
					</div>
	    		</div>
	    	</div>-->
	        <div class="row">
	            <div class="col-sm-12 col-md-9">
	                <div id="event-carousel" class="carousel slide" data-interval="false">
	                    <h2 class="heading">Musical Groups</h2>
	                    <a class="btn btn-primary pull_right" href="{!! route('musicalGroups.create') !!}">Add New</a>
	                                        
	                    <a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
	                    <a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
	                    @include('musicalGroups.table')
	                </div>
	            </div>
	            <div class="guitar">
	                <img class="img-responsive" src="http://localhost/musicalfestival/public/assets//images/guitar.png" alt="guitar">
	            </div>
	        </div>          
	    </div>
	</section><!--/#event-->

@endsection
