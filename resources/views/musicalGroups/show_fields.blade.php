<div class="row">
    <div class="col-sm-12 col-md-6 guitar2">               
        <img class="img-responsive" src="{!! $musicalGroup->image_url !!}" alt="guitar">
    </div>
    <div class="col-sm-12 col-md-6">
        <div class="about-content">                 
            <h2>{!! $musicalGroup->name !!}</h2>
            <p>{!! $musicalGroup->description !!}</p>
            <h2>Members</h2>            
            <div class="list-group">
                @foreach($musicalGroup->performers as $performer)
                    <div href="#" class="list-group-item">
                        <h3 class="list-group-item-heading">{!!$performer->name!!}</h3>
                        <h4 class="list-group-item-text">Instrument: {!!$performer->instrument->name!!}</h4>
                    </div>
                @endforeach                
            </div>
            <a class="btn btn-primary pull_right" href="{!! route('performers.create') !!}">Add New</a>                    
        </div>          
    </div>
</div>

