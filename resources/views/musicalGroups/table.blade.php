<div class="carousel-inner">
    <?php
        for($i=0; $i < count($musicalGroups); $i++){
            $mod = $i % 3;
            $routeShow = route('musicalGroups.show', [$musicalGroups[$i]->id]);

            if(($i !== 0) && ($mod === 0)){
                echo('
                        </div>
                    </div>');
            }

            if(($i === 0) || ($mod === 0)) {
                $class = ($i === 0) ? "item active" : "item";
                echo('
                    <div class="' . $class .'">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="single-event">
                                    <a href=" '. $routeShow .'">
                                        <img class="img-responsive" src="'. $musicalGroups[$i]->image_url .'" alt="event-image">
                                    </a>
                                    <h4>' . $musicalGroups[$i]->name . '</h4>
                                    <h5>' . $musicalGroups[$i]->genre->name . '</h5>
                                    <h6>' . $musicalGroups[$i]->city->name . '</h6>
                                </div>
                            </div>
                    ');
            } else {
                echo('
                    <div class="col-sm-4">
                        <div class="single-event">
                            <a href=" '. $routeShow .'">
                                <img class="img-responsive" src="'. $musicalGroups[$i]->image_url .'" alt="event-image">
                            </a>
                            <h4>' . $musicalGroups[$i]->name . '</h4>
                            <h5>'. $musicalGroups[$i]->genre->name . '</h5>
                            <h6>' . $musicalGroups[$i]->city->name . '</h6>
                        </div>
                    </div>
                    ');
            }

            if($i === count($musicalGroups) -1){
                echo('
                        </div>
                    </div>');
            }        
        }
    ?>
</div>

<!--<table class="table table-responsive" id="musicalGroups-table">
    <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Image Url</th>
        <th>Genre Id</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th>Deleted At</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($musicalGroups as $musicalGroup)
        <tr>
            <td>{!! $musicalGroup->name !!}</td>
            <td>{!! $musicalGroup->description !!}</td>
            <td>{!! $musicalGroup->image_url !!}</td>
            <td>{!! $musicalGroup->genre_id !!}</td>
            <td>{!! $musicalGroup->created_at !!}</td>
            <td>{!! $musicalGroup->updated_at !!}</td>
            <td>{!! $musicalGroup->deleted_at !!}</td>
            <td>
                {!! Form::open(['route' => ['musicalGroups.destroy', $musicalGroup->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('musicalGroups.show', [$musicalGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('musicalGroups.edit', [$musicalGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>-->
