@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit MusicalGroup</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($musicalGroup, ['route' => ['musicalGroups.update', $musicalGroup->id], 'method' => 'patch']) !!}

            @include('musicalGroups.fields')

            {!! Form::close() !!}
        </div>
@endsection
