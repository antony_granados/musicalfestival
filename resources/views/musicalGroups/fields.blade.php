<div class="form-group">    
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Nombre']) !!}
</div>
<div class="form-group">
    {!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => '5', 'placeholder'=>'Descripción']) !!}
</div>
<div class="form-group">
    {!! Form::file('image_url', ['class' => 'form-control', 'style' => 'background:none;border:none']) !!}
</div>
<div class="form-group">
    {!! Form::select('genre_id', $genres, 1, ['class' => 'form-control']) !!}    
</div>
<div class="form-group">
    {!! Form::select('city_id', $cities, 1, ['class' => 'form-control']) !!}    
</div>                        
<div class="form-group">    
    {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}
    <a href="{!! route('musicalGroups.index') !!}" class="btn btn-primary">Cancel</a>
</div>
