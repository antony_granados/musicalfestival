# Laravel PHP Framework

I developed a web application to show my knowledge to Prodigious Costa Rica.

Technical Data:

PHP Version: 7
-MySql: MariaDB 5.5
-Framework: Laravel 5
-Front-End: I used a template with Bootstrap, responsive design.
-Architecture: MVC
-I attached in the folder of this file a database diagram picture.

Requirements

Your task is to implement a single web page that showcases a Musical Festival in your
country. 
The main idea is to generate a simple version of a generic CRUD system.
The system must support multiple concurrent users.

The user must be able to see a list of all musical groups currently participating in the
Musical Festival. The listing will include the name of the musical group and their image.

The user can filter this listing by genre or city.

If a user clicks on the musical group image then they will be shown the musical group’s
profile, including the musical group description, individual members and their
instruments.

In addition, the system must allow users to add new performers and musical groups.

Please take into account the following guidelines and instructions:

Back End

You must use a framework such as Laravel or Symfony, or CMS platform such Drupal or Wordpress.

The platform must have a REST API that conforms to the Frontend specifications

Front End

The Frontend must obtain all data from the REST API
Ensure layout is preserved regardless of content
Ensure good practices and standards are applied
